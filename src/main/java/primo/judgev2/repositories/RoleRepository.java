package primo.judgev2.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import primo.judgev2.models.entities.Role;

public interface RoleRepository extends JpaRepository<Role, String> {
}
