package primo.judgev2.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import primo.judgev2.models.entities.Role;
import primo.judgev2.repositories.RoleRepository;
import primo.judgev2.services.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository repository;

    @Autowired
    public RoleServiceImpl(RoleRepository repository) {
        this.repository = repository;
    }

    @Override
    public void init() {
        if (repository.count() == 0) {
            Role userRole = new Role();
            Role adminRole = new Role();

            userRole.setName("USER");
            adminRole.setName("ADMIN");

            repository.saveAndFlush(userRole);
            repository.saveAndFlush(adminRole);
        }
    }
}
