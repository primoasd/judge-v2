package primo.judgev2.init;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import primo.judgev2.services.RoleService;

@Component
public class DatabaseInit implements CommandLineRunner {

    private final RoleService roleService;

    public DatabaseInit(RoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    public void run(String... args) throws Exception {
        this.roleService.init();
    }
}
